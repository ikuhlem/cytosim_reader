import os
import shutil
import tempfile
import subprocess
from cytosim_reader import CytosimReader

doc = """
Run tests from project root directory with: 
$ CYTOSIM_SIM=<path_to_sim_executable> CYTOSIM_REPORT=<path_to_report_executable> nosetests test/integration
Provide paths to the two cytosim executables sim and report_exe. nosetests is included in the python package nose.
"""

__doc__ = doc

if "CYTOSIM_SIM" not in os.environ:
    raise RuntimeError("sim executable not provided! " + __doc__)
sim = os.environ['CYTOSIM_SIM']
if "CYTOSIM_REPORT" not in os.environ:
    raise RuntimeError("report_exe executable not provided! " + __doc__)
report_exe = os.environ['CYTOSIM_REPORT']


def _generate_cytosim_data() -> tempfile.TemporaryDirectory:
    """
    Generates a temporary folder for a short simulation for
    integration tests.
    :return: Temporary simulation folder.
    """
    tmp_dir = tempfile.TemporaryDirectory()
    here = os.path.abspath(os.path.split(__file__)[0])
    config = os.path.join(tmp_dir.name, 'config.cym')
    shutil.copy(os.path.join(here, 'config.cym'), config)
    command_args = [sim]
    subprocess.call(command_args, cwd=tmp_dir.name)
    return tmp_dir


tmp_dir = _generate_cytosim_data()


def test_read_fiber():
    cym_reader = CytosimReader(tmp_dir.name, report_exe)
    report_identifier = 'fiber'
    fiber = cym_reader.read_report(report_identifier)
    for block in fiber:
        assert block.label == 'fiber'


def test_read_fiber_distribution():
    cym_reader = CytosimReader(tmp_dir.name, report_exe)
    report_identifier = 'fiber:distribution'
    fiber_dist = cym_reader.read_report(report_identifier)
    for block in fiber_dist:
        assert block.label == "fiber:distribution"


def test_read_fiber_points():
    cym_reader = CytosimReader(tmp_dir.name, report_exe)
    report_identifier = 'fiber:points'
    fiber_dist = cym_reader.read_report(report_identifier)
    for block in fiber_dist:
        assert block.label == "fiber:points"